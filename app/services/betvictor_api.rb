# frozen_string_literal: true

class BetvictorApi
  include Callable
  include HTTParty

  base_uri 'www.betvictor.com'
  PATH = '/api/sports.json'
  NAME_ATTR = 'description'

  def call
    response = self.class.get(PATH)
    return fallback_sports unless response.headers['content-type'].include?('application/json')

    JSON.parse(response.body)
  end

  private

  def fallback_sports
    [{ 'description' => 'Other', 'id' => -1 }]
  end
end
