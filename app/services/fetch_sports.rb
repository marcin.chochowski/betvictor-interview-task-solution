# frozen_string_literal: true

class FetchSports
  include Callable

  def initialize(sports_api)
    @sports_api = sports_api
  end

  def call
    Rails.cache.fetch('available_sports', expires_in: 2.hours) do
      sports_json = @sports_api.call
      sports_json.to_h { |sport| [sport['id'], sport[@sports_api::NAME_ATTR]] }
    end
  end
end
