# frozen_string_literal: true

class Market < ApplicationRecord
  belongs_to :event
  has_many :outcomes, dependent: :destroy

  accepts_nested_attributes_for :outcomes, allow_destroy: true,
                                           reject_if: ->(attrs) { attrs['result'].blank? || attrs['price'].blank? }

  def outcomes_hash
    outcomes.to_h do |outcome|
      [outcome.result, outcome.price]
    end
  end
end
