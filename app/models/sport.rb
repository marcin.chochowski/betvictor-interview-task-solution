# frozen_string_literal: true

class Sport < ApplicationRecord
  has_many :events, dependent: :destroy
end
