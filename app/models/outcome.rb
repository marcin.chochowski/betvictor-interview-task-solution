# frozen_string_literal: true

class Outcome < ApplicationRecord
  belongs_to :market
end
