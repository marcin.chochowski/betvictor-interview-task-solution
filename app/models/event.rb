# frozen_string_literal: true

class Event < ApplicationRecord
  belongs_to :sport
  has_many :markets, dependent: :destroy

  accepts_nested_attributes_for :markets, allow_destroy: true, reject_if: ->(attrs) { attrs['kind'].blank? }

  validates :name, presence: true, uniqueness: true
end
