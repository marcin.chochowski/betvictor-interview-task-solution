# frozen_string_literal: true

class EventsController < ApplicationController
  def index
    @events = Event.order(created_at: :desc).page(params[:page])
  end

  def new
    @event = Event.new
    @event.markets.build.outcomes.build
    @sports = FetchSports.call(BetvictorApi)
  end

  def create
    sport.events.create!(event_params)
    redirect_to events_path
  rescue ActiveRecord::RecordInvalid => e
    @event = e.record
    @event.markets.build.outcomes.build
    @sports = FetchSports.call(BetvictorApi)
    render :new
  end

  def update
    @event = event
    @event.update!(event_params.merge(sport: sport))
    redirect_to event_path(@event)
  rescue ActiveRecord::RecordInvalid => e
    @event = e.record
    @sports = FetchSports.call(BetvictorApi)
    render :edit
  end

  def show
    @event = event
  end

  def edit
    @event = event
    @sports = FetchSports.call(BetvictorApi)
  end

  private

  def event_params
    params.require(:event).permit(:name, markets_attributes: [:id, :kind, :_destroy,
                                                              { outcomes_attributes: %i[id result price _destroy] }])
  end

  def sport
    sport_id = params[:event][:external_sport_id].to_i
    sport_name = FetchSports.call(BetvictorApi)[sport_id]
    Sport.find_or_create_by(external_sport_id: sport_id, name: sport_name)
  end

  def event
    Event.find(params[:id])
  end
end
