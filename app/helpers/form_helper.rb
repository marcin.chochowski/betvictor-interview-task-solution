# frozen_string_literal: true

module FormHelper
  def error_for(form, field)
    return nil if form.object.errors[field].empty?

    "<p class='error'>#{form.object.errors[field].first}</p>"
  end

  def link_to_add_fields(name, form, association, secondary_association = nil)
    new_object = form.object.send(association).build
    new_object.send(secondary_association).build if secondary_association
    id = new_object.object_id
    fields = form.fields_for(association, new_object, child_index: id) do |builder|
      render("#{association.to_s.singularize}_fields", form: builder)
    end

    link_to(name, '#', class: 'add-fields unlinked btn btn-outline-primary', data: { id: id, fields: fields.gsub("\n", '') })
  end
end
