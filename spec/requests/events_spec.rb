# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Events' do
  before do
    sports = { 1100 => 'Handball', 1_327_866 => 'MMA/UFC', 1300 => 'Formula 1' }
    allow(FetchSports).to receive(:call).with(BetvictorApi).and_return(sports)
  end

  describe 'index' do
    let!(:events) { FactoryBot.create_list(:event, 3) }

    it 'responds with correct status' do
      get '/events'
      expect(response).to be_ok
    end

    it 'renders event names' do
      get '/events'
      expect(response.body).to include(*events.map(&:name))
    end
  end

  describe 'new' do
    it 'responds with correct status' do
      get '/events/new'
      expect(response).to be_ok
    end
  end

  describe 'edit' do
    let(:event) { FactoryBot.create(:event) }

    it 'responds with correct status' do
      get "/events/#{event.id}"
      expect(response).to be_ok
    end
  end

  describe 'create' do
    context 'with correct parameters' do
      let(:event_name) { Faker::Sports::Football.competition }
      let(:params) do
        { event: {
          name: event_name,
          external_sport_id: '1300',
          markets_attributes: [
            {
              kind: 'Exact score',
              outcomes_attributes: [
                { result: '0-0', price: '10/1' },
                { result: '0-1', price: '25/1' },
                { result: '1-0', price: '4/13' }
              ]
            },
            {
              kind: 'Match result',
              outcomes_attributes: [
                { result: '1', price: '2/11' },
                { result: 'X', price: '6/1' },
                { result: '2', price: '18/1' }
              ]
            }
          ]
        } }
      end

      it 'creates Event, Sport, Market and Outcome records' do
        expect do
          post '/events', params: params
        end.to change(Event, :count).by(1).and(change(Market, :count).by(2))
                                    .and(change(Outcome, :count).by(6))
                                    .and(change(Sport, :count).by(1))
      end

      it 'redirects correctly' do
        post '/events', params: params
        expect(response).to redirect_to(events_path)
      end

      context 'with existing sport selected' do
        let!(:sport) { FactoryBot.create(:sport, external_sport_id: 1300, name: 'Formula 1') }

        it 'creates Event, Market and Outcome records' do
          expect do
            post '/events', params: params
          end.to change(Event, :count).by(1).and(change(Market, :count).by(2))
                                      .and(change(Outcome, :count).by(6))
                                      .and(change(Sport, :count).by(0))
        end

        it 'assigns sport correctly' do
          post '/events', params: params
          expect(sport.reload.events.collect(&:name)).to eq([event_name])
        end
      end
    end

    context 'with empty name' do
      it 'does not create an Event record' do
        expect do
          post '/events', params: { event: { name: '' } }
        end.not_to change(Event, :count)
      end

      it 'renders error' do
        post '/events', params: { event: { name: '' } }
        expect(response.body).to include("can't be blank")
      end
    end
  end

  describe 'update' do
    let(:event_name) { Faker::Sports::Football.competition }
    let(:sport) { FactoryBot.create(:sport, external_sport_id: 1300, name: 'Formula 1') }
    let!(:event) { FactoryBot.create(:event, sport: sport, name: event_name) }

    context 'with correct parameters' do
      let(:new_event_name) { "#{event_name} - 2021" }
      let(:params) do
        { event: {
          name: new_event_name,
          external_sport_id: '1100'
        } }
      end

      it 'updates Event name and sport' do
        expect do
          put "/events/#{event.id}", params: params
        end.to change { event.reload.name }.to(new_event_name).from(event_name)
                                           .and(change { event.reload.sport.name }.to('Handball').from('Formula 1'))
      end

      it 'redirects correctly' do
        put "/events/#{event.id}", params: params
        expect(response).to redirect_to(event_path(event))
      end
    end

    context 'with empty name' do
      it 'does not create an Event record' do
        expect do
          put "/events/#{event.id}", params: { event: { name: '' } }
        end.not_to change(event, :name)
      end

      it 'renders error' do
        put "/events/#{event.id}", params: { event: { name: '' } }
        expect(response.body).to include("can't be blank")
      end
    end
  end
end
