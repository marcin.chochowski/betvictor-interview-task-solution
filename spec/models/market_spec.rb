# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Market do
  describe 'outcomes_hash' do
    subject(:market) { FactoryBot.create(:market) }

    let!(:outcome1) { FactoryBot.create(:outcome, market: market) }
    let!(:outcome2) { FactoryBot.create(:outcome, market: market) }

    it 'takes all outcome results as keys' do
      expect(market.outcomes_hash.keys).to eq([outcome1.result, outcome2.result])
    end

    it 'takes all outcome prices as values' do
      expect(market.outcomes_hash.values).to eq([outcome1.price, outcome2.price])
    end
  end

  describe 'outcomes' do
    subject(:market) { described_class.new }

    let(:outcomes_attributes) do
      [
        { result: '1', price: '2/11' },
        { result: '2', price: '18/1' }
      ]
    end

    it 'accepts outcomes attributes' do
      outcomes_attributes << { result: 'X', price: '6/1' }
      market.outcomes_attributes = outcomes_attributes
      market.save

      expect(market.outcomes_hash).to eq({ '1' => '2/11', '2' => '18/1', 'X' => '6/1' })
    end

    it 'rejects outcomes attributes if no price provided' do
      outcomes_attributes << { result: '3' }
      market.outcomes_attributes = outcomes_attributes
      market.save

      expect(market.outcomes_hash).to eq({ '1' => '2/11', '2' => '18/1' })
    end

    it 'rejects outcomes attributes if no result provided' do
      outcomes_attributes << { result: '', price: '123/1' }
      market.outcomes_attributes = outcomes_attributes
      market.save

      expect(market.outcomes_hash).to eq({ '1' => '2/11', '2' => '18/1' })
    end
  end
end
