# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Event do
  let(:sport) { FactoryBot.create(:sport) }

  describe 'validations' do
    subject(:event) { described_class.new(name: name, sport: sport) }

    context 'with correct attributes' do
      let(:name) { Faker::Sports::Football.competition }

      it 'is valid' do
        expect(event).to be_valid
      end
    end

    context 'with nil as name' do
      let(:name) { nil }

      it 'is not valid' do
        expect(event).not_to be_valid
      end
    end

    context 'with empty name' do
      let(:name) { '' }

      it 'is not valid' do
        expect(event).not_to be_valid
      end
    end

    context 'with whitespaces as name' do
      let(:name) { ' ' }

      it 'is not valid' do
        expect(event).not_to be_valid
      end
    end

    context 'with taken name' do
      let!(:existing_event) { FactoryBot.create(:event) }
      let(:name) { existing_event.name }

      it 'is not valid' do
        expect(event).not_to be_valid
      end
    end
  end

  describe 'markets' do
    subject(:event) { described_class.new }

    let(:market_name) { Faker::Lorem.sentence }
    let(:markets_attributes) do
      [{
        kind: market_name,
        outcomes_attributes: [
          { result: '1', price: '2/11' },
          { result: '2', price: '18/1' }
        ]
      }]
    end

    it 'accepts markets attributes' do
      event.markets_attributes = markets_attributes
      event.save
      expect(event.markets.map(&:kind)).to eq([market_name])
    end

    it 'rejects markets attributes if no kind provided' do
      markets_attributes << { kind: nil, outcomes_attributes: [] }
      event.markets_attributes = markets_attributes
      event.save

      expect(event.markets.map(&:kind)).to eq([market_name])
    end
  end
end
