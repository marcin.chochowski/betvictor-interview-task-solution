# frozen_string_literal: true

FactoryBot.define do
  factory :event do
    sport
    name { "#{Faker::Sports::Football.competition} (#{Time.zone.at(rand * 100.years.from_now.to_f).to_date})" }
  end
end
