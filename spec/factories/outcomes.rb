# frozen_string_literal: true

FactoryBot.define do
  factory :outcome do
    market
    result { "#{rand(1..5)}:#{rand(1..5)}" }
    price { "#{rand(2..99)}/#{rand(1..3)}" }
  end
end
