# frozen_string_literal: true

FactoryBot.define do
  factory :market do
    event
    kind { Faker::Lorem.sentence }
  end
end
