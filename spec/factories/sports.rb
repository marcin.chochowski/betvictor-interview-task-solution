# frozen_string_literal: true

FactoryBot.define do
  factory :sport do
    external_sport_id { rand(10_000..100_000) }
    name { Faker::Lorem.word }
  end
end
