# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'Events' do
  describe 'index' do
    let!(:events) { FactoryBot.create_list(:event, 3) }

    context 'with page size limited to 2' do
      let!(:original_page_size) { Kaminari.config.default_per_page }

      before { Kaminari.config.default_per_page = 2 }

      after { Kaminari.config.default_per_page = original_page_size }

      scenario 'user visits first page of events and it renders correct events and links' do
        visit '/events'
        expect(page).to have_css('.event').twice
        expect(page).to have_link(events[2].name)
        expect(page).to have_link(events[1].name)
        expect(page).to have_link('Next ›')
      end

      scenario 'user visits second page of events and it renders correct events and links' do
        visit '/events?page=2'
        expect(page).to have_css('.event').once
        expect(page).to have_link(events[0].name)
        expect(page).not_to have_link('Next ›')
        expect(page).to have_link('‹ Prev')
      end
    end

    scenario 'user visits events page and it shows Add event link' do
      visit '/events'
      expect(page).to have_link('Add event')
    end
  end

  describe 'user visits new event page' do
    let(:sports) { { 1100 => 'Handball', 1_327_866 => 'MMA/UFC', 1300 => 'Formula 1' } }

    before do
      allow(FetchSports).to receive(:call).with(BetvictorApi).and_return(sports)
      visit '/events/new'
    end

    scenario 'and it shows correct form' do
      expect(page).to have_field('Name')
      expect(page).to have_field('Type')
      expect(page).to have_field('Result')
      expect(page).to have_field('Price')
      expect(page).to have_button('Save')
    end

    scenario 'and creates event with markets and outcomes', js: true do
      event_name1 = Faker::Sports::Football.competition
      market_type1 = Faker::Lorem.sentence
      fill_in 'Name', with: event_name1
      select 'Formula 1', from: 'event_external_sport_id'
      fill_in 'Type', with: market_type1
      fill_in 'Result', with: '1'
      fill_in 'Price', with: '2/5'
      click_on 'Add next outcome'
      fill_in 'Result', with: 'X', currently_with: ''
      fill_in 'Price', with: '6/1', currently_with: ''
      click_on 'Add next outcome'
      fill_in 'Result', with: '2', currently_with: ''
      fill_in 'Price', with: '2/1', currently_with: ''
      click_on 'Add next market'
      market_type2 = Faker::Lorem.sentence
      fill_in 'Type', with: market_type2, currently_with: ''
      fill_in 'Result', with: '1:0', currently_with: ''
      fill_in 'Price', with: '11/2', currently_with: ''
      click_on 'Save'
      expect(page).to have_link(event_name1)
      click_on event_name1
      expect(page).to have_text(event_name1)
      expect(page).to have_text('Formula 1')
      expect(page).to have_text(market_type1)
      expect(page).to have_text('1')
      expect(page).to have_text('2/5')
      expect(page).to have_text('X')
      expect(page).to have_text('6/1')
      expect(page).to have_text('2')
      expect(page).to have_text('2/1')
      expect(page).to have_text(market_type2)
      expect(page).to have_text('1:0')
      expect(page).to have_text('11/2')
    end

    scenario 'and fills the form incorectly, then fixes the error and creates event' do
      fill_in 'Name', with: ''
      click_on 'Save'
      expect(page).to have_text("can't be blank")
      new_event_name = Faker::Sports::Football.competition
      fill_in 'Name', with: new_event_name
      click_on 'Save'
      expect(page).to have_link(new_event_name)
    end
  end

  # rubocop:disable RSpec/MultipleMemoizedHelpers
  describe 'user visits existing event page' do
    let(:event) { FactoryBot.create(:event) }
    let(:market1) { FactoryBot.create(:market, event: event) }
    let(:market2) { FactoryBot.create(:market, event: event) }
    let!(:outcome1) { FactoryBot.create(:outcome, market: market1) }
    let!(:outcome2) { FactoryBot.create(:outcome, market: market1) }
    let!(:outcome3) { FactoryBot.create(:outcome, market: market2) }
    let!(:outcome4) { FactoryBot.create(:outcome, market: market2) }

    scenario 'and it shows correct event, markets and outcomes details' do
      visit "/events/#{event.id}"
      expect(page).to have_text(event.name)
      expect(page).to have_text(market1.kind)
      expect(page).to have_text(market2.kind)
      expect(page).to have_text(outcome1.result)
      expect(page).to have_text(outcome1.price)
      expect(page).to have_text(outcome2.result)
      expect(page).to have_text(outcome2.price)
      expect(page).to have_text(outcome3.result)
      expect(page).to have_text(outcome3.price)
      expect(page).to have_text(outcome4.result)
      expect(page).to have_text(outcome4.price)
    end
  end
  # rubocop:enable RSpec/MultipleMemoizedHelpers

  describe 'user visits event page and clicks Edit' do
    let(:event_name) { Faker::Sports::Football.competition }
    let(:outcome) { FactoryBot.create(:outcome) }
    let(:market1) { FactoryBot.create(:market, outcomes: [outcome]) }
    let(:market2) { FactoryBot.create(:market) }
    let!(:event) do
      FactoryBot.create(:event, name: event_name, markets: [market1, market2],
                                sport: FactoryBot.create(:sport, external_sport_id: 1300, name: 'Formula 1'))
    end

    before do
      sports = { 1100 => 'Handball', 1_327_866 => 'MMA/UFC', 1300 => 'Formula 1' }
      allow(FetchSports).to receive(:call).with(BetvictorApi).and_return(sports)
      visit "/events/#{event.id}"
      click_on 'Edit'
    end

    scenario 'and it shows correct form' do
      expect(page).to have_field('Name')
      expect(page).to have_field('Type')
      expect(page).to have_field('Result')
      expect(page).to have_field('Price')
      expect(page).to have_button('Save')
    end

    scenario 'and updates event name and markets and outcomes', js: true do
      new_event_name = Faker::Sports::Football.competition
      new_market_type = Faker::Lorem.sentence
      fill_in 'Name', with: new_event_name, currently_with: event_name
      select 'Handball', from: 'event_external_sport_id'
      fill_in 'Type', with: new_market_type, currently_with: market1.kind
      fill_in 'Result', with: '1', currently_with: outcome.result
      fill_in 'Price', with: '2/7', currently_with: outcome.price
      click_on 'Add next outcome', near: page.find_field('Result', with: '1')
      fill_in 'Result', with: 'X', currently_with: ''
      fill_in 'Price', with: '8/1', currently_with: ''
      click_on 'Save'
      expect(page).to have_text(new_event_name)
      expect(page).to have_text('Handball')
      expect(page).to have_text(new_market_type)
      expect(page).to have_text('1')
      expect(page).to have_text('2/7')
      expect(page).to have_text('X')
      expect(page).to have_text('8/1')
    end

    scenario 'and fills the form incorectly, then fixes the error and creates event' do
      fill_in 'Name', with: '', currently_with: event_name
      click_on 'Save'
      expect(page).to have_text("can't be blank")
      new_event_name = Faker::Sports::Football.competition
      fill_in 'Name', with: new_event_name, currently_with: ''
      click_on 'Save'
      expect(page).to have_text(new_event_name)
    end

    scenario 'and updates event name and removes one market', js: true do
      new_event_name = Faker::Sports::Football.competition
      new_market_type = Faker::Lorem.sentence
      fill_in 'Name', with: new_event_name, currently_with: event_name
      select 'Handball', from: 'event_external_sport_id'
      fill_in 'Type', with: new_market_type, currently_with: market2.kind
      click_on 'X', above: page.find_field('Type', with: new_market_type)
      click_on 'Save'
      expect(page).to have_text(new_event_name)
      expect(page).to have_text('Handball')
      expect(page).to have_text(new_market_type)
      expect(page).not_to have_text(outcome.result)
      expect(page).not_to have_text(outcome.price)
      expect(page).not_to have_text(market1.kind)
    end
  end
end
