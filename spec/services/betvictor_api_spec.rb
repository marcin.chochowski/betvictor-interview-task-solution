# frozen_string_literal: true

require 'rails_helper'

RSpec.describe BetvictorApi do
  subject(:betvictor_api) { described_class.call }

  context 'when BetVictor API is available' do
    let(:sports) do
      [
        {
          "id": 1100,
          "event_path_id": 1100,
          "description": 'Handball',
          "url": '/sports/1100',
          "icon": 'sport-event-1100',
          "group": false
        }.with_indifferent_access,
        {
          "id": 1_327_866,
          "event_path_id": 1_327_866,
          "description": 'MMA/UFC',
          "url": '/sports/1327866',
          "icon": 'sport-event-1327866',
          "group": false
        }.with_indifferent_access,
        {
          "id": 1300,
          "event_path_id": 1300,
          "description": 'Formula 1',
          "url": '/sports/1300',
          "icon": 'sport-event-1300',
          "group": false
        }.with_indifferent_access
      ]
    end

    before do
      stub_request(:get, 'http://www.betvictor.com/api/sports.json')
        .to_return(status: 200, body: sports.to_json, headers: { content_type: 'application/json; charset=utf-8' })
    end

    it 'sends request to BetVictor API' do
      betvictor_api
      expect(WebMock).to have_requested(:get, 'http://www.betvictor.com/api/sports.json')
    end

    it 'returns parsed sports' do
      expect(betvictor_api).to eq(sports)
    end
  end

  context 'when BetVictor API is not available' do
    before do
      stub_request(:get, 'http://www.betvictor.com/api/sports.json')
        .to_return(status: 200, body: '<html>Unavailable</html>', headers: { content_type: 'text/html' })
    end

    it 'returns fallback sport' do
      expect(betvictor_api).to eq([{ 'id' => -1, 'description' => 'Other' }])
    end
  end
end
