# frozen_string_literal: true

require 'rails_helper'

RSpec.describe FetchSports do
  subject(:fetch_sports) { described_class.call(BetvictorApi) }

  let(:sports) do
    [
      {
        "id": 1100,
        "event_path_id": 1100,
        "description": 'Handball',
        "url": '/sports/1100',
        "icon": 'sport-event-1100',
        "group": false
      }.with_indifferent_access,
      {
        "id": 1_327_866,
        "event_path_id": 1_327_866,
        "description": 'MMA/UFC',
        "url": '/sports/1327866',
        "icon": 'sport-event-1327866',
        "group": false
      }.with_indifferent_access,
      {
        "id": 1300,
        "event_path_id": 1300,
        "description": 'Formula 1',
        "url": '/sports/1300',
        "icon": 'sport-event-1300',
        "group": false
      }.with_indifferent_access
    ]
  end

  before do
    allow(BetvictorApi).to receive(:call).and_return(sports)
  end

  it 'calls BetvictorApi' do
    fetch_sports
    expect(BetvictorApi).to have_received(:call)
  end

  it 'returns sports hash' do
    expect(fetch_sports).to eq({
                                 1100 => 'Handball',
                                 1_327_866 => 'MMA/UFC',
                                 1300 => 'Formula 1'
                               })
  end
end
