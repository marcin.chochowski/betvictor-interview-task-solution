# BetVictor Interview Task Solution 

by Marcin Chochowski

### Accessibility of betvictor.com in some countries

I wasn't able to find a reliable solution of sending the API request via proxy server located in a country where the service is available.

As a "production" kind of solution I would propose serving this app on a server located in for example Ireland.

For development it's fine to use a VPN. For test the API is mocked.
