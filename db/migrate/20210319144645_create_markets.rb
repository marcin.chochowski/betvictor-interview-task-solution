# frozen_string_literal: true

class CreateMarkets < ActiveRecord::Migration[6.0]
  def change
    create_table :markets do |t|
      t.string :kind
      t.references :event, null: false, foreign_key: true

      t.timestamps
    end
  end
end
