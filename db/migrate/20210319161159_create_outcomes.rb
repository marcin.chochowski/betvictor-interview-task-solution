# frozen_string_literal: true

class CreateOutcomes < ActiveRecord::Migration[6.0]
  def change
    create_table :outcomes do |t|
      t.references :market, null: false, foreign_key: true
      t.string :result
      t.string :price

      t.timestamps
    end
  end
end
