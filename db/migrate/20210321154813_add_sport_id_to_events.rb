# frozen_string_literal: true

class AddSportIdToEvents < ActiveRecord::Migration[6.0]
  def change
    add_reference :events, :sport, null: false, default: -1, foreign_key: true
  end
end
