# frozen_string_literal: true

class CreateSports < ActiveRecord::Migration[6.0]
  def change
    create_table :sports do |t|
      t.integer :external_sport_id
      t.string :name

      t.timestamps
    end
  end
end
