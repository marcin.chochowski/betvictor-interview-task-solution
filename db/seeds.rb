# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

%w[Football Handball Basketball MMA].each do |sport_name|
  FactoryBot.create(:sport, name: sport_name) do |sport|
    FactoryBot.create_list(:event, 10, sport: sport) do |event|
      FactoryBot.create_list(:market, 3, event: event) do |market|
        FactoryBot.create_list(:outcome, 5, market: market)
      end
    end
  end
end
